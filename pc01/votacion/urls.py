from django.urls import path
from . import views

app_name = 'votacion'

urlpatterns = [
    #ex: /encuesta/
    path('', views.index, name='index'),
    #ex: /encuesta/5/
    path('<int:region_id>/', views.detalle, name='detalle'),
    #ex: /encuesta/5/resultados/
    path('<int:region_id>/resultados/', views.resultados, name='resultados'),
    #ex: /encuesta/5/voto/
    path('<int:region_id>/voto/', views.votar, name='votar'),
]